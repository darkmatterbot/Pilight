package rwu.ss21.pilight.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import rwu.ss21.pilight.models.Server;
import rwu.ss21.pilight.service.repository.ServerRepository;
import rwu.ss21.pilight.service.rest.RetrofitManager;

import java.util.List;

public class ServerSettingVM extends AndroidViewModel {

    private ServerRepository server_repo;
    private LiveData<List<Server>> allServers;

    public ServerSettingVM(@NonNull Application application) {
        super(application);
        server_repo = ServerRepository.getInstance(application);
        allServers = server_repo.getAllservers();
    }
    public void insert(Server server){
        server_repo.insert(server);
    }
    public void update(Server server){
        server_repo.update(server);
    }
    public Server get_active_server(){
        List<Server> servers = allServers.getValue();
        if(servers != null) {
            for(int i=0;i<servers.size();i++){
                Server activeServer = servers.get(i);
                if(activeServer != null && activeServer.isActive()) {
                    //RetrofitManager.getInstance().setBASE_URL(activeServer.getIp(), String.valueOf(activeServer.getPort()));
                    return activeServer;
                }
            }
        }
        return null;
    }

    public boolean set_active_BASEURL(){
        Server s = get_active_server();
        if(s != null){
            RetrofitManager.getInstance().setBASE_URL(s.getIp(), String.valueOf(s.getPort()));
            return true;
        }
        return false;
    }
    public String get_active_server_name(){
        Server s = get_active_server();
        if(s!=null)
            return s.getName();
        else
            return "no server selected";
    }
    public void clear_active_server(){
        Server s = get_active_server();
        if(s !=null)
        {
            s.setActive(false);
            server_repo.update(s);
        }
    }
    public void setActiveServer(Server newActiveServer){
        clear_active_server();
        newActiveServer.setActive(true);
        update(newActiveServer);
    }
    public void delete(Server server){
        server_repo.delete(server);
    }
    public void deleteAllServers(){
        server_repo.deleteAllServers();
    }
    public LiveData<List<Server>> getAllServers(){
        return allServers;
    }
}
