package rwu.ss21.pilight.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import rwu.ss21.pilight.models.LED;
import rwu.ss21.pilight.service.repository.LEDRepository;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rwu.ss21.pilight.service.utils.Validator;

public class ColorPickerVM extends ViewModel implements LEDRepository.OnLED_ResponseListener, LEDRepository.OnLED_ErrorListener {

    private LED LED_model;
    private LEDRepository ledRepository;
    private MutableLiveData<LED> LIVEDATA;

    public ColorPickerVM() {
        LED_model = LED.getInstance();
        LIVEDATA = new MutableLiveData<>();

        ledRepository = LEDRepository.getInstance();

        // damit wird auf die Antwort vom Pi gelauscht
        ledRepository.setOnLED_ResponseListener(this);
        ledRepository.setOnLED_ErrorListener(this);
    }
    
    // ColorpickerViewFragment observiert dieses Element
    // und zeigt direkt jede Änderung im UI an
    public LiveData<LED> get_LED_LIVE_DATA() {
        return LIVEDATA;
    }

    private String get_color_padded(int color){
        if(color < 10)
            return "00"+color;
        else if(color < 100)
            return "0"+color;
        else
            return ""+color;
    }
    public void process_and_request_SelectedColor(int color){
        int r = ((color >> 16) & 255);
        int g = ((color >> 8) & 255);
        int b = color & 255;

        String hex_r = Validator.padLeftZeros(Integer.toHexString(r), 2);
        String hex_g = Validator.padLeftZeros(Integer.toHexString(g), 2);
        String hex_b = Validator.padLeftZeros(Integer.toHexString(b), 2);
        String color_in_hexa = hex_r + hex_g + hex_b;

        LED_model.setRed(get_color_padded(r));
        LED_model.setGreen(get_color_padded(g));
        LED_model.setBlue(get_color_padded(b));
        LED_model.setHex(color_in_hexa);
        LED_model.setCurrentColor(color);

        // repository beauftragen, die Anfrage an den PI zu schicken
        ledRepository.requestEffect("Color", color_in_hexa);
    }

    @Override  // Response Listener
    public void onLED_Response(Response<ResponseBody> response) {
        // success: 204 | error: 400
        LED_model.setResponse_code(response.code());
        LED_model.setResponse_msg(response.message());
        LIVEDATA.setValue(LED_model);
    }

    @Override // Error Listener
    public void onLED_Error(Throwable error) {
        LED_model.setResponse_code(404);
        LED_model.setResponse_msg(error.getMessage());
        LIVEDATA.setValue(LED_model);
    }
}