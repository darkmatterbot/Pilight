package rwu.ss21.pilight.viewmodel.schedules;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.service.repository.AlarmRepository;

public class DetailsVM extends ViewModel implements AlarmRepository.OnGetAlarm_ResponseListener, AlarmRepository.OnGetAlarm_ErrorListener {
    private AlarmRepository alarmRepository;
    private MutableLiveData<List<Alarm>> LIVEDATA;
    private static final String TAG = "DetailsVM";

    public DetailsVM() {
        LIVEDATA = new MutableLiveData<>();
        alarmRepository = AlarmRepository.getInstance();

        alarmRepository.setOnGetAlarm_ResponseListener(this);
        alarmRepository.setOnGetAlarm_ErrorListener(this);

        // Antwort kommt unten bei On_GetAlarm_Response irgendwann an
        alarmRepository.GETAlarms();
    }

    public LiveData<List<Alarm>> get_ALARM_LIVEDATA(){
        return LIVEDATA;
    }

    @Override
    public void onGetAlarm_Response(Response<List<Alarm>>  response) {
        LIVEDATA.setValue(response.body());
    }

    @Override
    public void onGetAlarm_Error(Throwable errorMsg) {
        LIVEDATA.setValue(null);
    }
}