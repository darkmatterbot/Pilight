package rwu.ss21.pilight.service.sqlite;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import rwu.ss21.pilight.models.Server;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Server.class}, version = 2, exportSchema = false)
public abstract class ServerDatabase extends RoomDatabase {

    public abstract ServerDao serverDao();

    // volatile für atomaren zugriff
    private static volatile ServerDatabase instance;

    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    // Singleton
    public static synchronized ServerDatabase getInstance(final Context context){
        if(instance==null)
        {
            synchronized (ServerDatabase.class) {
                if(instance == null){
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            ServerDatabase.class, "server_database")
                            // wenn das DB-Schema geändert wird, werden alle daten gelöscht, was hier ja nicht schlimm ist
                            .fallbackToDestructiveMigration()
                            // das würde direkt den Balkon-server unten einfügen
                            //.addCallback(roomDatabaseCallback)
                            .build();
                }
            }
        }
        return instance;
    }
    // Ab hier nur beispielhaftes Einfügen in die Server-DB
    private static Callback roomDatabaseCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            databaseWriteExecutor.execute(() -> {
                ServerDao dao = instance.serverDao();
                dao.insert(new Server("Balkon", "192.168.0.254", 666, false));
            });
        }
    };
}
