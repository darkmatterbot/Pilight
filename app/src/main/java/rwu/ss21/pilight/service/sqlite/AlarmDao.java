package rwu.ss21.pilight.service.sqlite;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import rwu.ss21.pilight.models.Alarm;

@Dao
public interface AlarmDao {
    @Insert
    void insert(Alarm alarm);

    @Update
    void update(Alarm alarm);

    @Delete
    void delete(Alarm alarm);

    // lets observe this list, our activity will be notified
    @Query("SELECT * FROM alarm_table")
    LiveData<List<Alarm>> getAllAlarms();


    @Query("DELETE FROM alarm_table")
    void deleteAllAlarms();

}
