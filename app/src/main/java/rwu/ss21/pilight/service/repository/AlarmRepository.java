package rwu.ss21.pilight.service.repository;

import android.util.Log;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.service.rest.PiApiRequest;
import rwu.ss21.pilight.service.rest.RetrofitManager;

public class AlarmRepository  {
    // volatile für atomaren zugriff, beides singletons
    private static volatile Retrofit retrofit;
    private static volatile PiApiRequest apiRequest;
    private static volatile AlarmRepository instance;

    private List<OnGetAlarm_ResponseListener> ALARM_ResponseListeners = new CopyOnWriteArrayList<>();;
    private List<OnGetAlarm_ErrorListener> ALARM_ErrorListeners = new CopyOnWriteArrayList<>();
    private static final String TAG = "AlarmRepository";
    private AlarmRepository(){
        apiRequest = getApi();
    }
    public static synchronized AlarmRepository getInstance(){
        if(instance==null)
        {
            synchronized (LEDRepository.class) {
                if(instance == null){
                    instance = new AlarmRepository();
                }
            }
        }
        return instance;
    }

    private PiApiRequest getApi(){
        retrofit = RetrofitManager.getInstance().getRetrofitInstance();
        return retrofit.create(PiApiRequest.class);
    }
    public void GETAlarms() {
        getApi().GETAlarms().enqueue(new Callback<List<Alarm>>() {
            @Override
            public void onResponse(Call<List<Alarm>> call, Response<List<Alarm>> alarmlist) {
                //Log.i(TAG, "GETAlarms: " +alarmlist.toString());
                if(ALARM_ResponseListeners != null){
                    Iterator<OnGetAlarm_ResponseListener> safeIterator = ALARM_ResponseListeners.iterator();

                    while (safeIterator.hasNext()) {

                        OnGetAlarm_ResponseListener listener_i = safeIterator.next();
                        listener_i.onGetAlarm_Response(alarmlist);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Alarm>> call, Throwable error) {
                //Log.i(TAG, "GETAlarms: " +error.getMessage());
                if(ALARM_ResponseListeners != null){
                    Iterator<OnGetAlarm_ErrorListener> safeIterator = ALARM_ErrorListeners.iterator();

                    while (safeIterator.hasNext()) {

                        OnGetAlarm_ErrorListener listener_i = safeIterator.next();
                        listener_i.onGetAlarm_Error(error);
                    }
                }
            }
        });
    }

    public void CreateAlarm(Alarm alarm){
        getApi().CreateAlarm(alarm).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "POSTAlarm: " +response.toString());
                GETAlarms();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "POSTAlarm: "+t.getMessage());
            }
        });
    }
    public void UpdateAlarm(Alarm alarm){
        getApi().UpdateAlarm(alarm.getId(), alarm).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "UpdateAlarm: " +response.toString());
                GETAlarms();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "UpdateAlarm: "+t.getMessage());
            }
        });
    }
    public void DELETEAlarm(int id){
        getApi().DELETEAlarm(id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "DELETEAlarm: " +response.toString());
                GETAlarms();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, "DELETEAlarm: " +t.getMessage());
            }
        });
    }


    // Antwort auf GET Alarms
    public interface OnGetAlarm_ResponseListener {
        void onGetAlarm_Response(Response<List<Alarm>> alarmlist);
    }
    public void setOnGetAlarm_ResponseListener(OnGetAlarm_ResponseListener listener){
        this.ALARM_ResponseListeners.add(listener);
    }
    public void removeOnGetAlarm_ResponseListener(OnGetAlarm_ResponseListener listener){
        this.ALARM_ResponseListeners.remove(listener);
    }

    // Error bei GET Alarms
    public interface OnGetAlarm_ErrorListener {
        void onGetAlarm_Error(Throwable errorMsg);
    }
    public void setOnGetAlarm_ErrorListener(OnGetAlarm_ErrorListener listener){
        this.ALARM_ErrorListeners.add(listener);
    }
    public void removeOnGetAlarm_ErrorListener(OnGetAlarm_ErrorListener listener){
        this.ALARM_ErrorListeners.remove(listener);
    }
}
