package rwu.ss21.pilight.service.utils;

import android.content.Context;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.models.Server;
import rwu.ss21.pilight.view.fragments.dialogs.SelectServerDialog;

public class Validator {

    public static Server isValidServer(TextView name, TextView ip, TextView port, Context context){
        // standard port nummer
        int s_port = 8888;

        // FALSCHE EINGABE? - START
        if(name.getText() == null || name.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please give your server a name", Toast.LENGTH_SHORT).show();
            return null;
        }

        if(ip.getText() == null || !Patterns.IP_ADDRESS.matcher(ip.getText()).matches())
        {
            Toast.makeText(context, "Please enter a valid IP Address", Toast.LENGTH_SHORT).show();
            return null;
        }
        if(port.getText() != null && !port.getText().toString().isEmpty())
        {
            s_port = Integer.parseInt(port.getText().toString());
            if(s_port < 1 || s_port > 65535) {
                Toast.makeText(context, "Port must be an Integer between 1 and 65535", Toast.LENGTH_SHORT).show();
                return null;
            }
        }
        // FALSCHE EINGABE? - ENDE

        Toast.makeText(context, "Added Server " +name.getText().toString() +" at "+ip.getText().toString()+" : "+s_port, Toast.LENGTH_SHORT).show();
        return  new Server(name.getText().toString(), ip.getText().toString(), s_port, false);
    }

    public static String get_days_from_code(int days){
        if(days == 0)
            return "Once";
        else if(days == 31)
            return "Week days";
        else if(days == 96)
            return "Weekends";
        else if(days == 127)
            return "Every day";

        String d[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
        StringBuilder daystring = new StringBuilder();

        for(int exp=0;exp<7;exp++){
            int compare_day = (int)(Math.pow(2,exp));
            if( (days & compare_day) == compare_day )
                daystring.append(d[exp] +" ");
        }

        return daystring.toString();
    }

    // Einzelne Hex-(RGB)-Werte links mit Nullen füllen, da sonst die zusammengesetzte Zahl nicht mehr stimmt
    public static String padLeftZeros(String inputString, int length) {
        int desiredLength = length - inputString.length();
        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < desiredLength) {
            sb.append('0');
        }
        sb.append(inputString);

        return sb.toString();
    }

    // ID - NAME - TYPE - TIME - DAYS - EFFECT - EFFECTPARAM - ENABLED
    public static Bundle get_standart_alarm(int type){
        return get_custom_alarm(0,"Alarm", type, "12:00", 0, "Rainbow", "50", true);
    }

    public static Bundle get_custom_alarm(int id, String name, int type, String time, int days, String effect, String effectparam, boolean enabled){
        Bundle b = new Bundle();
        b.putInt("id", id);
        b.putString("name", name);
        b.putInt("type", type);
        b.putString("time", time);
        b.putInt("days", days);
        b.putString("effect", effect);
        b.putString("effectparam", effectparam);
        b.putBoolean("enabled", enabled);

        return b;
    }

    public static int get_int_from_timestr(String time, int start, int stop){
        StringBuilder frag =new StringBuilder();
        char a = time.charAt(start);
        char b = time.charAt(stop);

        frag.append(a);
        frag.append(b);

        return Integer.parseInt(frag.toString());
    }
    public static String get_am_pm_format(int hour, int minute){
        String h, m=""+minute;

        if(minute < 10)
            m = "0"+minute;
        if(hour > 12)
            hour -=12;
        h = ""+hour;
        if(hour < 10)
            h = "0"+hour;
        return h +":"+m;
    }
    public static String get_am_pm(int hour){
        if(hour > 11)
            return "PM";
        return "AM";
    }

    public static boolean Clicker(MenuItem item, FragmentManager manager){
        if(item.getItemId() == R.id.select_active_server || item.getItemId() == R.id.active_server){
            SelectServerDialog dialog = SelectServerDialog.getInstance();
            if(!dialog.isAdded())
                dialog.show(manager, "Server Dropdown TAG2");
            return true;
        }
        return false;
    }

}
