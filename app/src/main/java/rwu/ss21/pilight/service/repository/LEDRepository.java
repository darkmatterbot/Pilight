package rwu.ss21.pilight.service.repository;

import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.models.Effect;
import rwu.ss21.pilight.service.rest.PiApiRequest;
import rwu.ss21.pilight.service.rest.RetrofitManager;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LEDRepository implements RetrofitManager.OnURLChangedListener, Callback {
    // volatile für atomaren zugriff, beides singletons
    private static volatile Retrofit retrofit;
    private static volatile PiApiRequest apiRequest;
    private static volatile LEDRepository instance;

    // Viewmodel implementiert die beiden Listeners und kann dadurch informiert werden,
    // sobald die Antwort der Asynchronen GET-Anfrage bereit steht
    private List<OnLED_ResponseListener> LED_responseListeners = new CopyOnWriteArrayList<>();;
    private List<OnLED_ErrorListener> LED_errorListeners = new CopyOnWriteArrayList<>();
    private RetrofitManager manager;

    //Singleton
    private LEDRepository(){
        manager = RetrofitManager.getInstance();
        manager.setOnURLChangedListener(this);
        retrofit = manager.getRetrofitInstance();
        apiRequest = retrofit.create(PiApiRequest.class);
    }

    public static synchronized LEDRepository getInstance(){
        if(instance==null)
        {
            synchronized (LEDRepository.class) {
                if(instance == null){
                    instance = new LEDRepository();
                }
            }
        }
        return instance;
    }

    @Override
    public void onURLChanged(Retrofit changedRetrofit) {
        retrofit = changedRetrofit;
        apiRequest = retrofit.create(PiApiRequest.class);
        apiRequest.GETGreeting().enqueue(this);
    }

    // hier wird die GET-Anfrage durchgeführt und sobald ein/e Antwort/Error hereinschneit,
    // wird das ColorpickerViewModel durch die Listener methoden informiert
    public void requestEffect(String effect, String effectParam){
        retrofit = RetrofitManager.getInstance().getRetrofitInstance();
        apiRequest = retrofit.create(PiApiRequest.class);
        apiRequest.POSTeffect(Effect.getInstance(effect, effectParam)).enqueue(this);
    }

    @Override
    public void onResponse(Call call, Response led_response) {
        if(this.LED_responseListeners!=null){
            Iterator<OnLED_ResponseListener> safeIterator = LED_responseListeners.iterator();

            while (safeIterator.hasNext()) {

                OnLED_ResponseListener listener_i = safeIterator.next();
                listener_i.onLED_Response(led_response);
            }
        }
    }
    @Override
    public void onFailure(Call call, Throwable error) {
        if(this.LED_errorListeners!=null){
            Iterator<OnLED_ErrorListener> safeIterator = LED_errorListeners.iterator();

            while (safeIterator.hasNext()) {
                OnLED_ErrorListener listener_i = safeIterator.next();
                listener_i.onLED_Error(error);
            }
        }
    }

    // Listener Methoden zum implementieren
    // Antwort
    public interface OnLED_ResponseListener {
        void onLED_Response(Response<ResponseBody> ledResponse);
    }
    public void setOnLED_ResponseListener(OnLED_ResponseListener listener){
        this.LED_responseListeners.add(listener);
    }
    public void removeOnLED_ResponseListener(OnLED_ErrorListener listener){
        this.LED_responseListeners.remove(listener);
    }
    // Fehler
    public interface OnLED_ErrorListener {
        void onLED_Error(Throwable errorMsg);
    }
    public void setOnLED_ErrorListener(OnLED_ErrorListener listener){
        this.LED_errorListeners.add(listener);
    }
    public void removeOnLED_ErrorListener(OnLED_ErrorListener listener){
        this.LED_errorListeners.remove(listener);
    }
}
