package rwu.ss21.pilight.service.repository;

import android.app.Application;

import androidx.lifecycle.LiveData;

import rwu.ss21.pilight.models.Server;
import rwu.ss21.pilight.service.sqlite.ServerDao;
import rwu.ss21.pilight.service.sqlite.ServerDatabase;

import java.util.List;

public class ServerRepository {
    private ServerDao serverDao;
    private LiveData<List<Server>> allservers;
    private static ServerRepository instance;

    //Singleton
    private ServerRepository(Application application){
        ServerDatabase serverDatabase = ServerDatabase.getInstance(application);
        serverDao = serverDatabase.serverDao();
        allservers = serverDao.getAllServers();
    }
    public static synchronized ServerRepository getInstance(Application application){
        if(instance==null)
        {
            synchronized (LEDRepository.class) {
                if(instance == null){
                    instance = new ServerRepository(application);
                }
            }
        }
        return instance;
    }

    // Neuer thread da sonst der main-thread einfriert
    public void insert(Server server){
        ServerDatabase.databaseWriteExecutor.execute( () -> {
            serverDao.insert(server);
        });
    }
    public void update(Server server){
        ServerDatabase.databaseWriteExecutor.execute( () -> {
            serverDao.update(server);
        });
    }

    public void delete(Server server){
        ServerDatabase.databaseWriteExecutor.execute( () -> {
            serverDao.delete(server);
        });
    }
    public void deleteAllServers(){
        ServerDatabase.databaseWriteExecutor.execute( () -> {
            serverDao.deleteAllServers();
        });
    }
    public LiveData<List<Server>> getAllservers() {
        return allservers;
    }
}
