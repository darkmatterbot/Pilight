package rwu.ss21.pilight.service.sqlite;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import rwu.ss21.pilight.models.Server;

import java.util.List;

@Dao
public interface ServerDao {
    @Insert
    void insert(Server server);

    @Update
    void update(Server server);

    @Delete
    void delete(Server server);

    // lets observe this list, our activity will be notified
    @Query("SELECT * FROM server_table")
    LiveData<List<Server>> getAllServers();


    @Query("DELETE FROM server_table")
    void deleteAllServers();
}
