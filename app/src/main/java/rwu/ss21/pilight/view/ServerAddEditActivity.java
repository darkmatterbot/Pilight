package rwu.ss21.pilight.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import rwu.ss21.pilight.R;

public class ServerAddEditActivity extends AppCompatActivity {
    public static final String EXTRA_ID= "com.example.viewmodel.EXTRA_ID";
    public static final String EXTRA_SERVERNAME = "com.example.viewmodel.EXTRA_SERVERNAME";
    public static final String EXTRA_SERVERIP = "com.example.viewmodel.EXTRA_SERVERIP";
    public static final String EXTRA_SERVERPORT = "com.example.viewmodel.EXTRA_SERVERPORT";
    private EditText add_server_name, add_server_ip, add_server_port;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Pilight);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_add_edit);

        add_server_name = findViewById(R.id.edit_servername);
        add_server_ip = findViewById(R.id.edit_ip);
        add_server_port = findViewById(R.id.edit_port);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();
        // if the intent has EXTRA_ID set, then the user wants to edit a server
        // set the previous values in the new intent
        if(intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit Server");
            String name = intent.getStringExtra(EXTRA_SERVERNAME);
            String ip = intent.getStringExtra(EXTRA_SERVERIP);
            String port = String.valueOf(intent.getIntExtra(EXTRA_SERVERPORT, 8888));
            add_server_name.setText(name);
            add_server_ip.setText(ip);
            add_server_port.setText(port);
        }
        else {
            // User wants to add a new server
            setTitle("Add Server");
        }
    }
    private void saveServer(){
        String name = add_server_name.getText().toString();
        String ip = add_server_ip.getText().toString();
        String p = add_server_port.getText().toString();
        if(p.trim().isEmpty())
            p = "8888";
        int port = Integer.parseInt(p);
        if(name.length() == 0 || ip.length() == 0){
            Toast.makeText(this, "Please add name and ip", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent data = new Intent();
        data.putExtra(EXTRA_SERVERNAME, name);
        data.putExtra(EXTRA_SERVERIP, ip);
        data.putExtra(EXTRA_SERVERPORT, port);
        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if(id != -1){
            // only set id if it is valid
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_server_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.save_server:
                saveServer();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}