package rwu.ss21.pilight.view.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import rwu.ss21.pilight.R;

public class ErrorInfoDialog extends DialogFragment implements View.OnClickListener {

    private AlertDialog dialog;
    private ConstraintLayout okField;
    private Button ok_btn;
    private String errorCause;
    private TextView errortextview;
    private Context context;

    public ErrorInfoDialog(String errorCause, Context c){
        this.errorCause = errorCause;
        this.context = c;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        View errorView = LayoutInflater.from(context).inflate(R.layout.dialog_error, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(errorView);

        okField = errorView.findViewById(R.id.ok_field);
        ok_btn = errorView.findViewById(R.id.error_ok_btn);
        errortextview = errorView.findViewById(R.id.errortext);

        errortextview.setText(this.errorCause);
        ok_btn.setOnClickListener(this);
        okField.setOnClickListener(this);


        dialog = builder.create();
        // Um die ecken abzurunden bzw unsichtbar zu machen, damit man die RUndungen sieht
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return dialog;
    }

    @Override
    public void onClick(View v) {
        dialog.dismiss();
    }
}
