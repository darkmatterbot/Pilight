package rwu.ss21.pilight.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Switch;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import rwu.ss21.pilight.R;

public class AppearanceActivity  extends AppCompatActivity implements  View.OnClickListener{
    private static OnDarkModeListener onDarkModeListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Pilight);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appearance);
        setTitle("Appearance");

        Switch darkmode_switch = findViewById(R.id.darkmode_switch);
        darkmode_switch.setOnClickListener(this);
        darkmode_switch.setChecked(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES);
    }

    @Override
    public void onClick(View v) {
        onDarkModeListener.onDarkModeChanged();
    }

    public interface OnDarkModeListener{
        void onDarkModeChanged();
    }
    public static void setOnNightModeListener(OnDarkModeListener listener){
        onDarkModeListener = listener;
    }
}
