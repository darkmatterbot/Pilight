package rwu.ss21.pilight.view;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.adapter.ServerAdapter;
import rwu.ss21.pilight.viewmodel.ServerSettingVM;
import rwu.ss21.pilight.models.Server;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ServerSettingActivity extends AppCompatActivity implements View.OnClickListener {

    // Um die intents unterscheiden zu können
    private static final int MYIDENTIFIER = 42;
    public static final int ADD_SERVER_REQUEST = 1;
    public static final int EDIT_SERVER_REQUEST = 2;

    private ServerSettingVM serverSettingVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Pilight);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_settings);
        setTitle("Servers");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);

        double randNumber = Math.random()*167111862;
        int randomInt = (int)randNumber;
        fab.setBackgroundTintList(ColorStateList.valueOf(-randomInt));

        // Recycler View
        RecyclerView recView = findViewById(R.id.server_rec_view);
        recView.setLayoutManager(new LinearLayoutManager(this));

        // Adapter
        final ServerAdapter adapter = new ServerAdapter(R.layout.item_server);
        recView.setAdapter(adapter);
        recView.setHasFixedSize(true);

        // Viewmodel
        serverSettingVM = new ViewModelProvider(this).get(ServerSettingVM.class);
        serverSettingVM.getAllServers().observe(this, servers -> adapter.submitList(servers));

        // zum swipen/löschen der server items
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback
                (0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                serverSettingVM.delete(adapter.getServerAt(viewHolder.getAdapterPosition()));
                String servername = adapter.getServerAt(viewHolder.getAdapterPosition()).getName();
                Toast.makeText(ServerSettingActivity.this, "Server " +servername +" deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recView);

        adapter.setOnServerClickListener((server) -> {
            Intent intent = new Intent(ServerSettingActivity.this, ServerAddEditActivity.class);
            intent.putExtra(ServerAddEditActivity.EXTRA_ID, server.getId());
            intent.putExtra(ServerAddEditActivity.EXTRA_SERVERNAME, server.getName());
            intent.putExtra(ServerAddEditActivity.EXTRA_SERVERIP, server.getIp());
            intent.putExtra(ServerAddEditActivity.EXTRA_SERVERPORT, server.getPort());
            startActivityForResult(intent, EDIT_SERVER_REQUEST);
        });
    }

    @Override // (+) FAB wurde geklickt -> user will neuen Server erstellen
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.fab:
                Intent intent = new Intent(ServerSettingActivity.this, ServerAddEditActivity.class);
                startActivityForResult(intent, ADD_SERVER_REQUEST);

                break;
            default: break;
        }
    }
    @Override // Server wurde hinzugefügt oder geändert oder user hat nur "x" geklickt
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode != RESULT_OK) {
            Toast.makeText(this, "server not saved", Toast.LENGTH_SHORT).show();
            return;
        }

        String servername = data.getStringExtra(ServerAddEditActivity.EXTRA_SERVERNAME);
        String ip = data.getStringExtra(ServerAddEditActivity.EXTRA_SERVERIP);
        int port = data.getIntExtra(ServerAddEditActivity.EXTRA_SERVERPORT, 8888);
        Server server = new Server(servername, ip, port, false);

        // Nutzer hat einen server hinzugefügt, wird hier nun gespeichert
        if(requestCode == ADD_SERVER_REQUEST ){
            serverSettingVM.insert(server);
            Toast.makeText(this, "server "+servername+" saved", Toast.LENGTH_SHORT).show();
        }
        // Nutzer hat einen server bearbeitet, wird hier nun geupdated
        else if(requestCode == EDIT_SERVER_REQUEST){
            int id = data.getIntExtra(ServerAddEditActivity.EXTRA_ID, -1);
            if(id == -1){
                Toast.makeText(this, "Server cant be updated", Toast.LENGTH_SHORT).show();
                return;
            }

            server.setId(id);
            serverSettingVM.update(server);
            Toast.makeText(this, "Server updated", Toast.LENGTH_SHORT).show();
        }
    }
}
