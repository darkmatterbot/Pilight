package rwu.ss21.pilight.view.fragments.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.view.AppearanceActivity;
import rwu.ss21.pilight.view.ServerSettingActivity;

import com.google.android.material.card.MaterialCardView;

public class SettingsFrag extends Fragment implements  View.OnClickListener {
    private View root;

    private MaterialCardView get_started, server, appearance, about;
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout._settings, container, false);

        get_started = root.findViewById(R.id.get_started_card);
        server = root.findViewById(R.id.server_card);
        appearance = root.findViewById(R.id.appearance_card);
        about = root.findViewById(R.id.about_card);

        get_started.setOnClickListener(this);
        server.setOnClickListener(this);
        appearance.setOnClickListener(this);
        about.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        NavController nav = Navigation.findNavController(root);
        switch(v.getId())
        {
            case R.id.get_started_card:
                nav.navigate(R.id.get_started);
                //intent = new Intent(getActivity(), GetStartedFragment.class);
                break;
            case R.id.server_card:
                intent = new Intent(getActivity(), ServerSettingActivity.class);
                break;
            case R.id.appearance_card:
                intent = new Intent(getActivity(), AppearanceActivity.class);
                break;
            case R.id.about_card:
                //intent = new Intent(getActivity(), AboutFragment.class);
                break;

            default: break;
        }
        if(intent!=null)
            startActivity(intent);
    }
}

