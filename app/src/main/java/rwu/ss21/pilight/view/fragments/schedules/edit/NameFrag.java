package rwu.ss21.pilight.view.fragments.schedules.edit;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.viewmodel.schedules.AddAlarmVM;

public class NameFrag extends Fragment {

    private static final String TAG ="NameFrag";
    private View nameView;
    private AddAlarmVM vm;
    private EditText nametxt;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        nameView= inflater.inflate(R.layout.schedule_details_add_label, container, false);
        nametxt = nameView.findViewById(R.id.nametxt);

        TextView title = nameView.findViewById(R.id.topbar_title);
        Button edit = nameView.findViewById(R.id.edit_item_btn);
        Button add = nameView.findViewById(R.id.add_item_btn);
        edit.setVisibility(View.GONE);
        add.setVisibility(View.GONE);
        title.setText("Label");

        return nameView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        vm = new ViewModelProvider(requireActivity()).get(AddAlarmVM.class);
        nametxt.setText(vm.getName().getValue());
        //Log.d(TAG, "VM: "+vm.toString());
    }

    @Override // Ein Icon oben in der Optionsbar wurde gedrückt
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        String name = nametxt.getText().toString();
        //Log.e(TAG, "back--> "+name);
        vm.setName(name);

        return super.onOptionsItemSelected(item);
    }
}