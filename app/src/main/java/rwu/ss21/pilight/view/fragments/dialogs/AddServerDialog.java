package rwu.ss21.pilight.view.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.models.Server;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.viewmodel.ServerSettingVM;
import com.google.android.material.button.MaterialButton;

public class AddServerDialog extends DialogFragment {
    private AlertDialog dialog;
    private ServerSettingVM serverSettingVM;
    private TextView name, ip, port;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        Context context = requireActivity();
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogview = inflater.inflate(R.layout.dialog_add_server, null);

        name = dialogview.findViewById(R.id.edit_servername);
        ip =  dialogview.findViewById(R.id.edit_ip);
        port =  dialogview.findViewById(R.id.edit_port);

        // ViewModelView
        serverSettingVM = new ViewModelProvider(this).get(ServerSettingVM.class);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogview);
        MaterialButton save_btn = dialogview.findViewById(R.id.save_btn);
        MaterialButton cancel_btn = dialogview.findViewById(R.id.cancel_btn);

        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        save_btn.setOnClickListener(v -> {
            Server new_server;
            if((new_server = Validator.isValidServer(name, ip, port, getActivity())) != null){
                serverSettingVM.insert(new_server);
                dialog.dismiss();
            }
        });
        cancel_btn.setOnClickListener(v -> {
            dialog.dismiss();
            SelectServerDialog dialog = SelectServerDialog.getInstance();
            if(!dialog.isAdded())
                dialog.show(getActivity().getSupportFragmentManager(), "Server Dropdown TAG");
        });

        return dialog;
    }
}
