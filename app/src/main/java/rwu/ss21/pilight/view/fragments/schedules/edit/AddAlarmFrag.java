package rwu.ss21.pilight.view.fragments.schedules.edit;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.service.repository.AlarmRepository;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.viewmodel.schedules.AddAlarmVM;

public class AddAlarmFrag extends Fragment implements View.OnClickListener{

    private static final String TAG ="AddAlarmFrag";

    private AddAlarmVM vm;
    private View alarmfragView;

    private Alarm alarm;
    private Bundle packet;
    private TimePicker timepicker;
    private TextView repeat_tv, name_tv, effect_tv;
    private ConstraintLayout repeat_layout, name_layout, effect_layout;
    private static boolean update = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        alarmfragView =  inflater.inflate(R.layout.schedule_details_add, container, false);
        repeat_layout = alarmfragView.findViewById(R.id.repeat_layout);
        name_layout = alarmfragView.findViewById(R.id.name_layout);
        effect_layout = alarmfragView.findViewById(R.id.effect_layout);

        timepicker = alarmfragView.findViewById(R.id.timePicker);

        repeat_tv = alarmfragView.findViewById(R.id.repeat_tv);
        name_tv = alarmfragView.findViewById(R.id.name_tv);
        effect_tv = alarmfragView.findViewById(R.id.effect_tv);

        vm = new ViewModelProvider(requireActivity()).get(AddAlarmVM.class);

        Button edit = alarmfragView.findViewById(R.id.edit_item_btn);
        Button add = alarmfragView.findViewById(R.id.add_item_btn);
        edit.setVisibility(View.GONE);
        add.setVisibility(View.GONE);

        packet = getArguments();
        alarm = new Alarm();

        // User wants to edit an alarm --> Data already present
        if(packet != null){
            if(!packet.isEmpty()) {
                update = true;
                //Log.i(TAG, "Editing --> " + packet.toString());
                vm.setId(packet.getInt("id"));
                vm.setName(packet.getString("name"));
                vm.setType(packet.getInt("type"));
                vm.setTime(packet.getString("time"));
                vm.setDays(packet.getInt("days"));
                vm.setEffect(packet.getString("effect"));
                vm.setEffectParam(packet.getString("effectparam"));
                vm.setEnabled(packet.getBoolean("enabled"));
                packet.clear(); // !! keep it clean for further use
            }
        }
        else
            update = false;

        //Log.e(TAG,vm.toString());
        // Die Addresse von vm sollte gleich sein wie die vm Addresse in der Klasse RepeatFrag!!
        return alarmfragView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        repeat_layout.setOnClickListener(this);
        name_layout.setOnClickListener(this);
        effect_layout.setOnClickListener(this);

        TextView title = alarmfragView.findViewById(R.id.topbar_title);
        String titles[] = {"Home & Away", "Morning Routine", "Sleeproutine", "Timer Routines", "Custom Routines"};
        title.setText(titles[vm.getType().getValue()-1]);

        vm.getId().observe(getViewLifecycleOwner(), id -> {
            //Log.i(TAG, "vm.getId: "+ id);
            alarm.setId(id);
        });

        vm.getName().observe(getViewLifecycleOwner(), name -> {
            //Log.i(TAG, "vm.getName: "+ name);
            name_tv.setText(name);
            alarm.setName(name);
        });

        vm.getType().observe(getViewLifecycleOwner(), type -> {
            //Log.i(TAG, "vm.getType: "+ type);
            alarm.setType(type);
        });

        vm.getTime().observe(getViewLifecycleOwner(), time -> {
            //Log.i(TAG, "vm.getTime: "+ time);
            int hour = Validator.get_int_from_timestr(time, 0,1);
            int minute = Validator.get_int_from_timestr(time, 3, 4);
            timepicker.setHour(hour);
            timepicker.setMinute(minute);
            alarm.setTime(time);
        });

        vm.getDays().observe(getViewLifecycleOwner(), days -> {
            //Log.i(TAG, "vm.getDays: " +days);
            alarm.setDays(days);
            repeat_tv.setText(Validator.get_days_from_code(days));
        });

        vm.getEffect().observe(getViewLifecycleOwner(), effect -> {
            //Log.i(TAG, "vm.getEffect: " +effect);
            effect_tv.setText(effect);
            alarm.setEffect(effect);
        });

        vm.getEffectParam().observe(getViewLifecycleOwner(), effectparam -> {
            alarm.setEffectparam(effectparam);
            //Log.i(TAG, "vm.getEffectparam: "+effectparam);
        });

        vm.getEnabled().observe(getViewLifecycleOwner(), enabled -> {
            alarm.setEnabled(enabled);
        });
    }

    @Override
    public void onClick(View v) {
        NavController nav = Navigation.findNavController(alarmfragView);
        set_time();

        switch(v.getId()){
            case R.id.repeat_layout:
                nav.navigate(R.id.to_schedule_edit_repeat);
                break;
            case R.id.name_layout:
                nav.navigate(R.id.to_schedule_edit_name);
                break;

            case R.id.effect_layout:
                nav.navigate(R.id.to_schedule_edit_effect);
                break;
            default: break;
        }
    }


    private void set_time(){
        int hour = timepicker.getHour();
        int minute = timepicker.getMinute();
        String h=""+hour, m=""+minute;

        if(hour < 10)
            h = "0"+hour;
        if(minute < 10)
            m = "0"+minute;
        String t = ""+h+":"+m;
        vm.setTime(t);
    }
    @Override // SETTINGS Icon oben rechts wurde geklickt
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.save){

            set_time();
            if(update)
                AlarmRepository.getInstance().UpdateAlarm(alarm);
            else
                AlarmRepository.getInstance().CreateAlarm(alarm);
            Navigation.findNavController(alarmfragView).navigateUp();// Eine Ebene zurück

            //Log.i(TAG, "ALARM: id: {"+alarm.getId() +"}, name: {"+alarm.getName()+"}, type: {"+alarm.getType()+"}, time: {"+alarm.getTime()+"}, days: {"+alarm.getDays()+"}, effect: {"+alarm.getEffect()+"}, effectparam: {"+alarm.getEffectparam()+"}, enabled: {"+alarm.getEnabled()+"}");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save, menu);
    }
}