package rwu.ss21.pilight;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import rwu.ss21.pilight.service.repository.LEDRepository;
import rwu.ss21.pilight.service.utils.Validator;
import rwu.ss21.pilight.view.AppearanceActivity;
import rwu.ss21.pilight.view.fragments.ColorPickerFrag;
import rwu.ss21.pilight.view.fragments.schedules.edit.AddEffectFrag;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AppearanceActivity.OnDarkModeListener, ColorPickerFrag.OnColorChangedListener2, AddEffectFrag.OnColorChangedListener, View.OnLongClickListener {

    private FloatingActionButton fab;
    boolean isEnabled = false;
    private static boolean darkmode = true;
    private  AppBarConfiguration appBarConfiguration;
    private static final String TAG ="MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Pilight);
        if(darkmode){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else
        {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout._activity_main);
        BottomNavigationView navView = findViewById(R.id.bottom_navigation_view);
        navView.setBackground(null);
        navView.getMenu().getItem(2).setEnabled(false);

        // Die Bottom-Navigation-Bar, alle icons werden als top-level ziele angesehen
        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_lighteffects,R.id.navigation_colorpicker, R.id.navigation_schedules, R.id.navigation_settings)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);


        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);
        fab.setOnLongClickListener(this);
        AppearanceActivity.setOnNightModeListener(this);

        ColorPickerFrag.setOnColorChangedListener2(this);
        AddEffectFrag.setOnColorChangedListener(this);

    }

    // Zurück Button oben Links POP UP TO
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private String hex_from_int(int color){
        int r = ((color >> 16) & 255);
        int g = ((color >> 8) & 255);
        int b = color & 255;

        String hex_r = Validator.padLeftZeros(Integer.toHexString(r), 2);
        String hex_g = Validator.padLeftZeros(Integer.toHexString(g), 2);
        String hex_b = Validator.padLeftZeros(Integer.toHexString(b), 2);
        return hex_r + hex_g + hex_b;
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fab:

                double randNumber = Math.random()*167111862;
                int randomInt = (int)randNumber;
                fab.setBackgroundTintList(ColorStateList.valueOf(-randomInt));
                String hex = hex_from_int(-randomInt);
                LEDRepository.getInstance().requestEffect("Color", hex);
                isEnabled = !isEnabled;
                break;
            default: break;
        }
    }
    @Override
    public boolean onLongClick(View v) {
        fab.setBackgroundTintList(ColorStateList.valueOf(-6908265));
        LEDRepository.getInstance().requestEffect("Off", "");
        return true;
    }
    @Override
    public void onDarkModeChanged() {
        darkmode = !darkmode;
        recreate();
    }

    @Override
    public void onColorChanged2(int color) {
        fab.setBackgroundTintList(ColorStateList.valueOf(color));
    }

    @Override
    public void onColorChanged(int color) {
        fab.setBackgroundTintList(ColorStateList.valueOf(color));
    }

}