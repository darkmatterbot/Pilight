package rwu.ss21.pilight.adapter;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.switchmaterial.SwitchMaterial;

import org.jetbrains.annotations.NotNull;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.models.Alarm;
import rwu.ss21.pilight.service.repository.AlarmRepository;
import rwu.ss21.pilight.service.utils.Validator;

public class AlarmAdapter extends ListAdapter<Alarm, AlarmAdapter.AlarmHolder>  {

    private int SELECTED_LAYOUT = -1;
    private static final String TAG ="AlarmAdapter";
    private static boolean editMode = false;

    public AlarmAdapter(int layout_id){
        super(DIFF_CALLBACK);
        SELECTED_LAYOUT=layout_id;
    }
    private static final DiffUtil.ItemCallback<Alarm> DIFF_CALLBACK = new DiffUtil.ItemCallback<Alarm>() {
        @Override
        public boolean areItemsTheSame(@NonNull Alarm oldItem, @NonNull Alarm newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Alarm oldItem, @NonNull Alarm newItem) {
            return oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getDays() == oldItem.getDays() &&
                    oldItem.getEffect().equals(newItem.getEffect()) &&
                    oldItem.getEffectparam().equals(newItem.getEffectparam()) &&
                    oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getTime().equals(newItem.getTime()) &&
                    oldItem.getType() == newItem.getType();
        }
    };

    @NonNull
    @NotNull
    @Override
    public AlarmHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        if(SELECTED_LAYOUT < 0)
        {
            //Log.e(TAG, "SELECTED LAYOUT IS -1");
            return null;
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(SELECTED_LAYOUT, parent, false);
        return new AlarmHolder(itemView);
    }

    public boolean isEditing(){
        editMode = !editMode;
        notifyDataSetChanged();
        return editMode;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AlarmHolder holder, int position) {
        Alarm currentAlarm = getItem(position);
        String t = currentAlarm.getTime();

        /* Vom 24 Zeitformat zu AM-PM Format */
        int hour = Validator.get_int_from_timestr(t, 0,1);
        int minute = Validator.get_int_from_timestr(t, 3, 4);
        String time = Validator.get_am_pm_format(hour, minute);
        String am_pm = Validator.get_am_pm(hour);
        /**/

        holder.name.setText(currentAlarm.getName());
        holder.time.setText(time);
        holder.daynight.setText(am_pm);
        holder.days.setText(Validator.get_days_from_code(currentAlarm.getDays()));
        holder.effect.setText("Effect: "+currentAlarm.getEffect());
        holder.on_off.setId(currentAlarm.getId());
        holder.on_off.setChecked(currentAlarm.getEnabled());

        if(editMode) {
            holder.hidden_layer.setVisibility(View.VISIBLE);
            holder.on_off.setVisibility(View.GONE);
            holder.edit.setVisibility(View.VISIBLE);
        }
        else{
            holder.hidden_layer.setVisibility(View.GONE);
            holder.on_off.setVisibility(View.VISIBLE);
            holder.edit.setVisibility(View.GONE);
        }
    }

    public Alarm getAlarmAt(int i){
        return getItem(i);
    }

    class AlarmHolder extends RecyclerView.ViewHolder {
        private TextView name, time, daynight, days, effect;
        private SwitchMaterial on_off;
        private ConstraintLayout hidden_layer;
        private Button edit;
        private Chip delete;



        public AlarmHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            hidden_layer = itemView.findViewById(R.id.hidden_alarm_layer);
            delete = itemView.findViewById(R.id.alarm_delete_chip);
            edit = itemView.findViewById(R.id.edit_alarm_btn);
            name = itemView.findViewById(R.id.alarm_name);
            time = itemView.findViewById(R.id.alarm_time);
            daynight = itemView.findViewById(R.id.alarm_daynight);
            days = itemView.findViewById(R.id.alarm_days);
            effect = itemView.findViewById(R.id.alarm_effect);
            on_off = itemView.findViewById(R.id.alarm_on_off);
            on_off.setOnClickListener(v -> {
                int pos = getAdapterPosition();
                if( pos != RecyclerView.NO_POSITION){
                    Alarm clickedAlarm = getAlarmAt(pos);
                    if(clickedAlarm!=null){
                        boolean enabled = clickedAlarm.getEnabled();
                        clickedAlarm.setEnabled(!enabled);
                        Log.i(TAG, "ALARM ID: "+clickedAlarm.getId());
                        AlarmRepository.getInstance().UpdateAlarm(clickedAlarm);
                    }
                    else{
                        Log.e(TAG, "ALARM IS NULL");
                    }

                }

            });
            delete.setOnClickListener( v -> {
                int i = getAdapterPosition();
                Alarm alarmtobecrushed = getAlarmAt(i);
                if(alarmtobecrushed!=null){
                    AlarmRepository.getInstance().DELETEAlarm(alarmtobecrushed.getId());
                }
                else{
                    Log.e(TAG, "alarmtobecrushed IS NULL");
                }
                //Log.i(TAG, "Deleting alarm: "+alarmtobecrushed.getName());
            });
            itemView.setOnClickListener(v -> {
                //Log.i(TAG, "Editing mode: "+editMode);

                if(editMode) {
                    Alarm a = getAlarmAt(getAdapterPosition());
                    NavController nav = Navigation.findNavController(itemView);
                    Bundle b = Validator.get_custom_alarm(a.getId(), a.getName(), a.getType(), a.getTime(), a.getDays(), a.getEffect(), a.getEffectparam(), a.getEnabled());
                    nav.navigate(R.id.add_alarm_frag,b);
                    editMode = !editMode;
                }
                else {
                    hidden_layer.setVisibility(View.GONE);
                    on_off.setVisibility(View.VISIBLE);
                    edit.setVisibility(View.GONE);
                }
            });
        }
    }

}
