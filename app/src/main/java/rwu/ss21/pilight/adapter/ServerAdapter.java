package rwu.ss21.pilight.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import rwu.ss21.pilight.R;
import rwu.ss21.pilight.models.Server;

public class ServerAdapter extends ListAdapter<Server, ServerAdapter.ServerHolder>{

    private int SELECTED_LAYOUT = -1;
    private int selectedServerPosition = -1;
    private OnServerClickListener serverListener;

    public ServerAdapter(int layout_id)
    {
        super(DIFF_CALLBACK);
        SELECTED_LAYOUT=layout_id;
    }
    private static final DiffUtil.ItemCallback<Server> DIFF_CALLBACK = new DiffUtil.ItemCallback<Server>() {
        @Override
        public boolean areItemsTheSame(@NonNull Server oldItem, @NonNull Server newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Server oldItem, @NonNull Server newItem) {
            return oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getIp().equals(oldItem.getIp()) &&
                    oldItem.getPort() == newItem.getPort();
        }
    };

    @NonNull
    @Override
    public ServerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(SELECTED_LAYOUT < 0)
        {
            Log.e("MAYDAY", "SELECTED LAYOUT IS -1");
            return null;
        }
        View itemview = LayoutInflater.from(parent.getContext())
                .inflate(SELECTED_LAYOUT, parent, false);

        return new ServerHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull ServerHolder holder, int position) {
        Server currentServer = getItem(position);
        holder.servername.setText(currentServer.getName());
        holder.serverip.setText(currentServer.getIp());
        holder.serverport.setText(String.valueOf(currentServer.getPort()));
        if(SELECTED_LAYOUT == R.layout.item_server_radio){
            holder.radiobtn.setChecked(currentServer.getId() == selectedServerPosition);
           // holder.radiobtn.setPressed(currentServer.getId() == selectedServerPosition);
        }

    }

    public Server getServerAt(int position){
        return getItem(position);
    }

    class ServerHolder extends RecyclerView.ViewHolder {

        private TextView servername, serverip, serverport;
        private RadioButton radiobtn;

        public ServerHolder(View itemView) {
            // das ist die Cardview
            super(itemView);
            servername = itemView.findViewById(R.id.server_name);
            serverip = itemView.findViewById(R.id.server_ip);
            serverport = itemView.findViewById(R.id.server_port);
            radiobtn = itemView.findViewById(R.id.radiobtn);

            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (serverListener != null && position != RecyclerView.NO_POSITION) {
                    serverListener.onServerClick(getItem(position));
                }
            });
        }
    }
    public void itemCheckChanged(int tag){
        selectedServerPosition = tag;
        notifyDataSetChanged();
    }
    public interface OnServerClickListener {
        void onServerClick(Server server);
    }

    public void setOnServerClickListener(OnServerClickListener listener){
        this.serverListener = listener;
    }
}
